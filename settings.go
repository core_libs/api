package api

import (
	"gitlab.com/core_libs/api/auth"
	"gitlab.com/core_libs/api/types"
)

type settingsApi struct {
	setBasicAuth bool

	dbFuncWorker              types.DbFuncWorker
	customValidateMessageFunc types.ValidateMessageFunc
}

func initSettings() *settingsApi {
	return &settingsApi{
		setBasicAuth: false,
	}
}

func (a *fastApi) EnableBasicAuth() {
	a.settings.setBasicAuth = true
	a.authMiddlewares[auth.BASIC] = auth.NewBasicAuth(a.appCfg.BasicLogin, a.appCfg.BasicPassword).Middleware
}

func (a *fastApi) SetDbFuncWorker(dbFuncWorker types.DbFuncWorker) {
	a.settings.dbFuncWorker = dbFuncWorker
}

func (a *fastApi) SetCustomValidateMessageFunc(validateMessageFunc types.ValidateMessageFunc) {
	a.settings.customValidateMessageFunc = validateMessageFunc
}
