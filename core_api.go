package api

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/core_libs/api/auth"
	"gitlab.com/core_libs/api/rest_data"
)

type authMiddleware map[auth.AuthType]func(ctx *gin.Context)

type fastApi struct {
	appCfg          *appConfig
	settings        *settingsApi
	authMiddlewares authMiddleware

	r *gin.Engine
}

var api *fastApi

func NewApi() *fastApi {
	newApp := new(fastApi)
	appCfg := getConfig()

	newApp.appCfg = appCfg
	newApp.authMiddlewares = createMiddlewares()
	newApp.settings = initSettings()

	if !appCfg.Debug {
		gin.SetMode(gin.ReleaseMode)
		gin.DisableConsoleColor()
	}
	r := gin.Default()
	r.Use(cors())
	newApp.r = r

	api = newApp
	return newApp
}

func (a *fastApi) bindRequest(ctx *gin.Context, data interface{}, binding func(obj any) error, serviceDataKey string, serviceData *map[string]interface{}) error {
	err := binding(data)
	if err != nil {
		rest_data.BindValidationErrorFromErrorWithAbort(ctx, err, a.settings.customValidateMessageFunc)
		return err
	}
	(*serviceData)[serviceDataKey] = data
	return nil
}

func createMiddlewares() authMiddleware {
	am := make(authMiddleware, 1)

	am[auth.OPEN] = func(c *gin.Context) {
		c.Next()
	}

	return am
}

func (a *fastApi) Run() {
	a.checkAppConf()
	log.Println("Start app on port: ", a.appCfg.Port)
	err := a.r.Run(fmt.Sprintf(":%s", a.appCfg.Port))
	if err != nil {
		log.Fatalf("Error start server; err: %s", err.Error())
	}
}
