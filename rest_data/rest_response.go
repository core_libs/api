package rest_data

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/core_libs/api/types"
)

const bindRestData = "RestData"

const (
	validationError   = "validation_error"
	notFoundError     = "not_found_error"
	vadRequestError   = "bad_request_error"
	forbiddenError    = "forbidden_error"
	unauthorizedError = "unauthorized_error"
)

func render(ctx *gin.Context) {
	data, ok := ctx.Get(bindRestData)
	r, _ := data.(RestData)
	if !ok {
		ctx.Status(http.StatusNoContent)
		return
	}
	ctx.JSON(r.HttpStatusCode, r)
}

func BindRestData(ctx *gin.Context, data RestData) {
	ctx.Set(bindRestData, data)
	if data.HasError() {
		ctx.Abort()
	}
	render(ctx)
}

func BindCreated(ctx *gin.Context, messageData *json.RawMessage) {
	BindRestData(ctx, RestData{
		HttpStatusCode: http.StatusCreated,
		Data:           messageData,
	})
}

func BindOk(ctx *gin.Context, messageData *json.RawMessage) {
	BindRestData(ctx, RestData{
		HttpStatusCode: http.StatusOK,
		Data:           messageData,
	})
}

func BindOkDefault(ctx *gin.Context, data interface{}) {
	bindDefault(ctx, http.StatusOK, data)
}

func BindCreatedDefault(ctx *gin.Context, data interface{}) {
	bindDefault(ctx, http.StatusCreated, data)
}

func bindDefault(ctx *gin.Context, status int, data interface{}) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Println("[BindOkDefault] error marshal; details -", err)
		return
	}
	response := json.RawMessage(jsonData)
	rd := RestData{
		HttpStatusCode: status,
		Data:           &response,
		Errors:         nil,
	}
	BindRestData(ctx, rd)
}

func BindNoContent(ctx *gin.Context) {
	BindRestData(ctx, RestData{
		HttpStatusCode: http.StatusNoContent,
	})
}

func BindValidationErrorFromErrorWithAbort(ctx *gin.Context, err error, customValidatorMessage types.ValidateMessageFunc) {
	var ve validator.ValidationErrors
	var rd RestData
	if customValidatorMessage == nil {
		customValidatorMessage = getErrorMsg
	}
	rd.HttpStatusCode = http.StatusBadRequest
	if errors.As(err, &ve) {
		out := make([]CustomError, len(ve))
		for i, fe := range ve {
			field := fieldNameEdit(fe.Field())
			out[i] = CustomError{
				ErrorKey: field,
				Message:  validationError,
				Detail:   customValidatorMessage(fe),
			}
		}
		rd.Errors = out
		BindRestData(ctx, rd)
		return
	}
	bindCustomErrorWithAbort(ctx, rd.HttpStatusCode, nil, "error_params", validationError, "Неверные параметры запроса")
}

func BindBadRequestWithAbort(ctx *gin.Context, key, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusBadRequest, nil, key, vadRequestError, detail)
}

func BindBusinessErrorWithAbort(ctx *gin.Context, key string, message string, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusUnprocessableEntity, nil, key, message, detail)
}

func BindBusinessErrorWithDataWithAbort(ctx *gin.Context, data any, key string, message string, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusUnprocessableEntity, data, key, message, detail)
}

func BindUnauthorizedErrorWithAbort(ctx *gin.Context, key string, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusUnauthorized, nil, key, unauthorizedError, detail)
}

func BindNotFoundWithAbort(ctx *gin.Context, key, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusNotFound, nil, key, notFoundError, detail)
}

func BindForbiddenWithAbort(ctx *gin.Context, key, detail string) {
	bindCustomErrorWithAbort(ctx, http.StatusNotFound, nil, key, forbiddenError, detail)
}

func bindCustomErrorWithAbort(ctx *gin.Context, errCode int, data any, errKey, msg, detail string) {
	var rd RestData

	rawData, ok := data.(*json.RawMessage)
	if ok {
		rd.Data = rawData
	}

	rd = RestData{
		HttpStatusCode: errCode,
		Errors: []CustomError{
			{
				ErrorKey: errKey,
				Message:  msg,
				Detail:   detail,
			},
		},
	}
	BindRestData(ctx, rd)
	ctx.Abort()
}
