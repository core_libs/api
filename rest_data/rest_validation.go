package rest_data

import (
	"strings"

	"github.com/go-playground/validator/v10"
)

func fieldNameEdit(f string) string {
	return strings.ToLower(f[:1]) + f[1:]
}

func getErrorMsg(fe validator.FieldError) string {
	switch fe.Tag() {
	case "required":
		return "Поле должно быть обязательным параметром"
	case "email":
		return "Неверный формат Email"
	case "min":
		switch fe.Value().(type) {
		case int:
			return "Минимальное значение поля должно быть " + fe.Param()
		default:
			return "Минимальная длина поля должна быть " + fe.Param()
		}
	case "max":
		switch fe.Value().(type) {
		case int:
			return "Максимальное значение поля должно быть " + fe.Param()
		default:
			return "Максимальная длина поля должна быть " + fe.Param()
		}
	case "e164":
		return "Неверный формат номера телефона"
	case "len":
		return "Длина поля должна быть " + fe.Param()
	case "numeric":
		return "Поле должно состоять из цифр"
	case "oneof":
		return "Поле должно содержать одно из этих значений: [" + strings.Replace(fe.Param(), " ", ", ", -1) + "]"
	case "gt":
		return "Значение поля должно больше " + fe.Param()
	case "gte":
		return "Значение поля должно больше или равно " + fe.Param()
	case "lt":
		return "Значение поля должно меньше " + fe.Param()
	case "lte":
		return "Значение поля должно меньше или равно " + fe.Param()
	case "in":
		return "Значение поля должно быть равно одному из " + fe.Param()
	}
	return "Неверные параметры запроса"
}
