package rest_data

import (
	"encoding/json"
	"net/http"
)

type RequestType string

const (
	GET    RequestType = "GET"
	POST               = "POST"
	PUT                = "PUT"
	PATCH              = "PATCH"
	DELETE             = "DELETE"
)

type RestData struct {
	HttpContentType int              `json:"-"`
	HttpStatusCode  int              `json:"-"`
	Data            *json.RawMessage `json:"data,omitempty"`
	Errors          []CustomError    `json:"errors,omitempty"`
}

func (restData RestData) Error() string {
	return http.StatusText(restData.HttpStatusCode)
}

func (restData RestData) HasError() bool {
	return len(restData.Errors) > 0
}

func (restData RestData) DataEmpty() bool {
	return restData.Data == nil
}

func (restData RestData) HasErrorOrDataEmpty() bool {
	return restData.HasError() || restData.DataEmpty()
}

func (restData RestData) GetData() []byte {
	if restData.Data != nil {
		return *restData.Data
	}
	return nil
}
