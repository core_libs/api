package rest_data

type CustomError struct {
	ErrorKey string `json:"error,omitempty"`
	Message  string `json:"message,omitempty"`
	Detail   string `json:"detail,omitempty"`
}

func NewCustomError(key, message, detail string, statusCode int) RestData {
	return newRestErrorData(key, message, detail, statusCode)
}

func newRestErrorData(key, message, detail string, statusCode int) RestData {
	var restData RestData
	localErr := CustomError{
		ErrorKey: key,
		Message:  message,
		Detail:   detail,
	}

	restData.Errors = append(restData.Errors, localErr)
	restData.HttpStatusCode = statusCode
	return restData
}
