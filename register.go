package api

import "reflect"

func (a *fastApi) RegisterDefault(route, base, procedureName string) *defaultApiConf {
	return newDefaultApiConf(route, base, procedureName)
}

func (a *fastApi) RegisterService(route string, service apiService) *serviceApiConf {
	return newServiceApiConf(route, service)
}

func (a *fastApi) copyStruct(src interface{}) interface{} {
	val := reflect.ValueOf(src)
	newInstance := reflect.New(val.Type()).Interface()
	return newInstance
}
