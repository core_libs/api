package api

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/core_libs/api/auth"
	"gitlab.com/core_libs/api/rest_data"
	"gitlab.com/core_libs/api/types"
)

type apiService func(ctx *gin.Context, data map[string]interface{})

type serviceApiConf struct {
	auth        auth.AuthType
	method      rest_data.RequestType
	route       string
	query       interface{}
	body        interface{}
	params      interface{}
	middlewares []gin.HandlerFunc
	service     apiService
}

func newServiceApiConf(route string, service apiService) *serviceApiConf {
	return &serviceApiConf{
		route:       route,
		method:      rest_data.GET,
		auth:        auth.OPEN,
		middlewares: make([]gin.HandlerFunc, 0),
		service:     service,
	}
}

func (s *serviceApiConf) Get() *serviceApiConf {
	s.method = rest_data.GET
	return s
}
func (s *serviceApiConf) Post() *serviceApiConf {
	s.method = rest_data.POST
	return s
}
func (s *serviceApiConf) Patch() *serviceApiConf {
	s.method = rest_data.PATCH
	return s
}
func (s *serviceApiConf) Put() *serviceApiConf {
	s.method = rest_data.PUT
	return s
}
func (s *serviceApiConf) Delete() *serviceApiConf {
	s.method = rest_data.DELETE
	return s
}

func (s *serviceApiConf) Open() *serviceApiConf {
	s.auth = auth.OPEN
	return s
}
func (s *serviceApiConf) Basic() *serviceApiConf {
	s.auth = auth.BASIC
	return s
}

func (s *serviceApiConf) Query(query interface{}) *serviceApiConf {
	s.query = query
	return s
}
func (s *serviceApiConf) Body(body interface{}) *serviceApiConf {
	s.body = body
	return s
}
func (s *serviceApiConf) Params(params interface{}) *serviceApiConf {
	s.params = params
	return s
}

func (s *serviceApiConf) Middleware(middleware gin.HandlerFunc) *serviceApiConf {
	s.middlewares = append(s.middlewares, middleware)
	return s
}

func (s *serviceApiConf) Reg() {
	api.regService(s)
}

func (a *fastApi) regService(cfg *serviceApiConf) {
	controller := func(ctx *gin.Context) {
		serviceData := make(map[string]interface{})

		if cfg.params != nil {
			params := a.copyStruct(cfg.params)
			err := a.bindRequest(ctx, params, ctx.ShouldBindUri, types.PARAMS, &serviceData)
			if err != nil {
				return
			}
		}

		if cfg.query != nil {
			query := a.copyStruct(cfg.query)
			err := a.bindRequest(ctx, query, ctx.ShouldBindQuery, types.QUERY, &serviceData)
			if err != nil {
				return
			}
		}

		if cfg.body != nil {
			body := a.copyStruct(cfg.body)
			err := a.bindRequest(ctx, body, ctx.ShouldBindJSON, types.BODY, &serviceData)
			if err != nil {
				return
			}
		}

		cfg.service(ctx, serviceData)
	}

	am, ok := a.authMiddlewares[cfg.auth]
	if !ok {
		log.Fatalf("Unknown auth type [%s]", cfg.route)
		return
	}
	var handlers []gin.HandlerFunc
	if len(cfg.middlewares) != 0 {
		h := make([]gin.HandlerFunc, 0, 4)
		h = append(h, append([]gin.HandlerFunc{am}, cfg.middlewares...)...)
		h = append(h, controller)
		handlers = h
	} else {
		handlers = append(handlers, am, controller)
	}

	switch cfg.method {
	case rest_data.GET:
		a.r.GET(cfg.route, handlers...)
	case rest_data.POST:
		a.r.POST(cfg.route, handlers...)
	case rest_data.PUT:
		a.r.PUT(cfg.route, handlers...)
	case rest_data.PATCH:
		a.r.PATCH(cfg.route, handlers...)
	case rest_data.DELETE:
		a.r.DELETE(cfg.route, handlers...)
	default:
		log.Fatalf("Unknown request method [%s]", cfg.method)
		return
	}
}
