package auth

type AuthType int

const (
	JWT AuthType = iota
	TMA
	BASIC
	OPEN
)
