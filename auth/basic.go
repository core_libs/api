package auth

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/core_libs/api/rest_data"
)

type basicMiddleware struct {
	login    string
	password string
}

func NewBasicAuth(login string, password string) *basicMiddleware {
	return &basicMiddleware{login: login, password: password}
}

func (m *basicMiddleware) Middleware(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	if authHeader == "" {
		rest_data.BindUnauthorizedErrorWithAbort(ctx, "wrong_header", "Неверный формат заголовка")
		return
	}

	usernameProvided, passwordProvided, ok := ctx.Request.BasicAuth()
	if !ok || usernameProvided != m.login || passwordProvided != m.password {
		rest_data.BindUnauthorizedErrorWithAbort(ctx, "wrong_data", "Неверный логин или пароль")
		return
	}

	ctx.Next()
}
