package auth

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
)

const UserDataCtx = "user_data_context"

func GetUserData(ctx *gin.Context) (map[string]interface{}, error) {
	userData, ok := ctx.Get(UserDataCtx)
	if !ok {
		return nil, fmt.Errorf("user data is empty")
	}

	user, ok := userData.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("wrong format user data")
	}

	return user, nil
}

func GetUserDataWithMap[T interface{}](ctx *gin.Context) (T, error) {
	var user T
	userData, ok := ctx.Get(UserDataCtx)
	if !ok {
		return user, fmt.Errorf("user data is empty")
	}

	userJson, err := json.Marshal(userData)
	if err != nil {
		return user, err
	}

	if err = json.Unmarshal(userJson, &user); err != nil {
		return user, err
	}
	return user, nil
}