package types

import "github.com/go-playground/validator/v10"

type ValidateMessageFunc func(fe validator.FieldError) string
