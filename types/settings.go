package types

import "context"

type DbFuncWorker func(ctx context.Context, name string, procedureName string, params ...interface{}) (interface{}, error)
