package api

import (
	"encoding/json"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/core_libs/api/auth"
	"gitlab.com/core_libs/api/rest_data"
	"gitlab.com/core_libs/api/types"
)

type defaultApiConf struct {
	withUserData  bool
	auth          auth.AuthType
	method        rest_data.RequestType
	route         string
	base          string
	procedureName string
	query         interface{}
	body          interface{}
	params        interface{}
	middlewares   []gin.HandlerFunc
}

func newDefaultApiConf(route, base, procedureName string) *defaultApiConf {
	return &defaultApiConf{
		route:         route,
		procedureName: procedureName,
		method:        rest_data.GET,
		auth:          auth.OPEN,
		base:          base,
		middlewares:   make([]gin.HandlerFunc, 0),
	}
}

func (d *defaultApiConf) Get() *defaultApiConf {
	d.method = rest_data.GET
	return d
}
func (d *defaultApiConf) Post() *defaultApiConf {
	d.method = rest_data.POST
	return d
}
func (d *defaultApiConf) Patch() *defaultApiConf {
	d.method = rest_data.PATCH
	return d
}
func (d *defaultApiConf) Put() *defaultApiConf {
	d.method = rest_data.PUT
	return d
}
func (d *defaultApiConf) Delete() *defaultApiConf {
	d.method = rest_data.DELETE
	return d
}

func (d *defaultApiConf) Open() *defaultApiConf {
	d.auth = auth.OPEN
	return d
}
func (d *defaultApiConf) Basic() *defaultApiConf {
	d.auth = auth.BASIC
	return d
}

func (d *defaultApiConf) Query(query interface{}) *defaultApiConf {
	d.query = query
	return d
}
func (d *defaultApiConf) Body(body interface{}) *defaultApiConf {
	d.body = body
	return d
}
func (d *defaultApiConf) Params(params interface{}) *defaultApiConf {
	d.params = params
	return d
}

func (d *defaultApiConf) Middleware(middleware gin.HandlerFunc) *defaultApiConf {
	d.middlewares = append(d.middlewares, middleware)
	return d
}

func (d *defaultApiConf) Reg() {
	api.regDefault(d)
}

func (a *fastApi) regDefault(cfg *defaultApiConf) {
	controller := func(ctx *gin.Context) {
		serviceData := make(map[string]interface{})
		procedureData := make([]interface{}, 0)

		if cfg.withUserData {
			userData, err := auth.GetUserData(ctx)
			if err != nil {
				log.Printf("[ERROR] RegisterDefault json.Marshal userData | Details: %s", err)
				rest_data.BindUnauthorizedErrorWithAbort(ctx, "user_data", "Некорректные данные пользователя")
				return
			}
			if len(userData) != 0 {
				userDataJSON, errMarshal := json.Marshal(userData)
				if errMarshal != nil {
					log.Printf("[ERROR] RegisterDefault json.Marshal userData | Details: %s", errMarshal)
					rest_data.BindUnauthorizedErrorWithAbort(ctx, "user_data", "Некорректные данные пользователя")
					return
				}
				procedureData = append(procedureData, string(userDataJSON))
			}
		}

		if cfg.params != nil {
			params := a.copyStruct(cfg.params)
			err := a.bindRequest(ctx, params, ctx.ShouldBindUri, types.PARAMS, &serviceData)
			if err != nil {
				return
			}

			paramsJSON, err := json.Marshal(serviceData[types.PARAMS])
			if err != nil {
				log.Printf("[ERROR] RegisterDefault json.Marshal PARAMS | Details: %s", err.Error())
				rest_data.BindBadRequestWithAbort(ctx, "params_data", "Неверные параметры запроса")
				return
			}
			procedureData = append(procedureData, string(paramsJSON))
		}

		if cfg.query != nil {
			query := a.copyStruct(cfg.query)
			err := a.bindRequest(ctx, query, ctx.ShouldBindQuery, types.QUERY, &serviceData)
			if err != nil {
				return
			}
			queryJSON, err := json.Marshal(serviceData[types.QUERY])
			if err != nil {
				log.Printf("[ERROR] RegisterDefault json.Marshal QUERY | Details: %s", err.Error())
				rest_data.BindBadRequestWithAbort(ctx, "query_data", "Неверные параметры запроса")
				return
			}
			procedureData = append(procedureData, string(queryJSON))
		}

		if cfg.body != nil {
			body := a.copyStruct(cfg.body)
			err := a.bindRequest(ctx, body, ctx.ShouldBindJSON, types.BODY, &serviceData)
			if err != nil {
				return
			}
			bodyJSON, err := json.Marshal(serviceData[types.BODY])
			if err != nil {
				log.Printf("[ERROR] RegisterDefault json.Marshal BODY | Details: %s", err.Error())
				rest_data.BindBadRequestWithAbort(ctx, "body_data", "Неверные параметры запроса")
				return
			}
			procedureData = append(procedureData, string(bodyJSON))
		}

		res, err := a.settings.dbFuncWorker(ctx.Request.Context(), cfg.base, cfg.procedureName, procedureData...)
		if err != nil {
			log.Printf("[ERROR] RegisterDefault Bases.Get fronm [%s] | Details: %s", cfg.route, err.Error())
			rest_data.BindBadRequestWithAbort(ctx, "request", "Ошибка запроса. Повторите попытку позднее")
			return
		}

		resJSON, err := json.Marshal(res)
		if err != nil {
			log.Printf("[ERROR] RegisterDefault json.Marshal [%s] | Details: %s", cfg.route, err.Error())
			rest_data.BindBadRequestWithAbort(ctx, "request", "Ошибка запроса. Повторите попытку позднее")
		}

		response := json.RawMessage(resJSON)
		switch cfg.method {
		case rest_data.GET:
			rest_data.BindOk(ctx, &response)
		case rest_data.POST:
			rest_data.BindCreated(ctx, &response)
		case rest_data.PUT:
			rest_data.BindCreated(ctx, &response)
		case rest_data.PATCH:
			rest_data.BindCreated(ctx, &response)
		case rest_data.DELETE:
			rest_data.BindCreated(ctx, &response)
		default:
			rest_data.BindOk(ctx, &response)
		}
	}

	am, ok := a.authMiddlewares[cfg.auth]
	if !ok {
		log.Fatalf("Unknown auth type [%s]", cfg.route)
		return
	}
	var handlers []gin.HandlerFunc
	if len(cfg.middlewares) != 0 {
		h := make([]gin.HandlerFunc, 0, 4)
		h = append(h, append([]gin.HandlerFunc{am}, cfg.middlewares...)...)
		h = append(h, controller)
		handlers = h
	} else {
		handlers = append(handlers, am, controller)
	}

	switch cfg.method {
	case rest_data.GET:
		a.r.GET(cfg.route, handlers...)
	case rest_data.POST:
		a.r.POST(cfg.route, handlers...)
	case rest_data.PUT:
		a.r.PUT(cfg.route, handlers...)
	case rest_data.PATCH:
		a.r.PATCH(cfg.route, handlers...)
	case rest_data.DELETE:
		a.r.DELETE(cfg.route, handlers...)
	default:
		log.Fatalf("Unknown request method [%s]", cfg.method)
		return
	}
}
