package api

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/core_libs/api/rest_data"
	"gitlab.com/core_libs/config"
)

const (
	appConfigName = "app"
)

type appConfig struct {
	Port        string `env:"APP_PORT"`
	Debug       bool   `env:"APP_DEBUG"`
	PingHandler bool   `env:"APP_PING_HANDLER"`
	ServiceName string `env:"SERVICE_NAME"`

	BasicLogin    string `env:"APP_BASIC_LOGIN"`
	BasicPassword string `env:"APP_BASIC_PASSWORD"`
}

func getConfig() *appConfig {
	if !config.CheckSecretConfig(appConfigName) {
		panic("config app is missing")
	}
	appCfg, err := config.GetSecretConfig[appConfig](appConfigName)
	if err != nil {
		panic(err)
	}
	return &appCfg
}

func (a *fastApi) checkAppConf() {
	a.confHandlers()
}

func (a *fastApi) confHandlers() {
	if a.appCfg.PingHandler {
		a.addPing()
		log.Println("[api] Ping active; route [/ping]")
	} else {
		log.Println("[api] Ping is disabled; To enable it, set the [APP_PING_HANDLER = true]")
	}
}

func (a *fastApi) addPing() {
	pingHandler := func(ctx *gin.Context) {
		rest_data.BindNoContent(ctx)
	}
	a.r.GET("ping", pingHandler)
}
